"""
    @author Николай Витальевич Никоноров (Bitnik212)
    @date 02.03.2023 22:03
"""
import json

from aiohttp import ClientResponse, ClientSession
from aiohttp.client import _RequestContextManager as RequestContextManager

from reels_api_client.common import Logger
from reels_api_client.common.error.RenewTokensError import RenewTokensError
from reels_api_client.common.model.JWTsModel import JWTsModel
from reels_api_client.main.APIContext import APIContext
from reels_api_client.main.IJWTsCallback import IJWTsCallback


class AuthInterceptor:

    def __init__(self, context: APIContext):
        self.logger = Logger(context)
        self.__client: ClientSession = context.session
        self.__callback: IJWTsCallback = context.jwt_callback
        self.__tokens = context.tokens

    async def __call__(self, request_func, *args, **kwargs) -> RequestContextManager:
        first_response = await request_func(*args, **kwargs)
        self.logger.debug(f"first response: {await self.logger.parse_response(first_response)}")
        if first_response.status == 403:
            await self.renew_tokens()
        return await request_func(*args, **kwargs)

    async def renew_tokens(self) -> ClientResponse:
        response = await self.__client.post(
            url="/token/refresh",
            data=json.dumps({
                "refresh_token": self.__tokens.refresh_token
            }),
            headers=self.__client.headers.update(
                {
                    "Content-Type": "application/json"
                }
            )
        )
        response_data = await response.json()
        self.logger.debug(f"renew_tokens. response.status: {response.status}, response_data: {response_data}")
        if response.status == 200:
            data = response_data["data"]
            self.__tokens = JWTsModel(access_token=data.get("access", ""), refresh_token=data.get("refresh", ""))
            self.__callback.on_update_tokens(self.__tokens)
        else:
            raise RenewTokensError(response_data["info"])
        return response
