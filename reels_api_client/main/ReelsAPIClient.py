"""
    @author Николай Витальевич Никоноров (Bitnik212)
    @date 02.03.2023 21:45
"""
import aiohttp

from reels_api_client.common.CustomClientSession import CustomClientSession
from reels_api_client.common.model.JWTsModel import JWTsModel
from reels_api_client.main.APIContext import APIContext
from reels_api_client.main.IJWTsCallback import IJWTsCallback
from reels_api_client.main.ReelsAPI import ReelsAPI
from reels_api_client.main.UserReelsAPI import UserReelsAPI


class ReelsAPIClient:

    user: UserReelsAPI | None = None
    reels: ReelsAPI | None = None

    def __init__(self, base_url: str, tokens: JWTsModel, jwt_callback: IJWTsCallback | None = None, debug: bool = False):
        context = APIContext(
                session=CustomClientSession(
                    tokens=tokens,
                    jwt_callback=jwt_callback,
                    base_url=base_url,
                    headers={"Authorization": f"Bearer {tokens.access_token}"} if tokens.access_token is not None else None
                ),
                tokens=tokens,
                jwt_callback=jwt_callback,
                debug=debug
        )
        self.reels = ReelsAPI(context)
        self.user = UserReelsAPI(context)
