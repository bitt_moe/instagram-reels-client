"""
    @author Николай Витальевич Никоноров (Bitnik212)
    @date 02.03.2023 22:20
"""
from reels_api_client.common.model.JWTsModel import JWTsModel


class IJWTsCallback:

    def on_update_tokens(self, tokens: JWTsModel):
        print("Not implemented")
