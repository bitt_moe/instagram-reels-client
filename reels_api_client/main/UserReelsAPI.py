"""
    @author Николай Витальевич Никоноров (Bitnik212)
    @date 02.03.2023 21:48
"""
import json

import aiohttp

from reels_api_client.common.model.JWTsModel import JWTsModel


class UserReelsAPI:

    @classmethod
    async def sign_in(cls, base_url: str, login: str, password: str) -> JWTsModel | None:
        async with aiohttp.ClientSession(base_url=base_url, headers={"Content-Type": "application/json"}) as session:
            async with session.post(url="/user/sign/in", data=json.dumps({"email": login, "password": password})) as response:
                print(f"sign_in:\nurl: {response.url}, \nstatus: {response.status}.")
                response_data = await response.json()
                if response.status == 200:
                    data = response_data["data"]
                    return JWTsModel(
                        access_token=data["access"],
                        refresh_token=data["refresh"]
                    )
                else:
                    raise Exception(response_data["info"])

    @classmethod
    async def sign_up(cls, base_url: str, login: str, password: str, ig_session_id: str) -> JWTsModel | None:
        async with aiohttp.ClientSession(base_url=base_url, headers={"Content-Type": "application/json"}) as session:
            async with session.post(
                    url="/user/sign/up",
                    data=json.dumps({
                        "email": login,
                        "password": password,
                        "repeat_password": password,
                        "ig_session_id": ig_session_id
                    })
            ) as response:
                print(f"sign_up:\nurl: {response.url}, \nstatus: {response.status}.")
                response_data = await response.json()
                if response.status == 200:
                    data = response_data["data"]
                    return JWTsModel(
                        access_token=data["access"],
                        refresh_token=data["refresh"]
                    )
                else:
                    raise Exception(response_data["info"])

    @classmethod
    async def edit(cls, base_url: str, access_token: str, login: str | None = None, password: str | None = None, ig_session_id: str | None = None) -> bool:
        async with aiohttp.ClientSession(base_url=base_url, headers={
            "Content-Type": "application/json",
            "Authorization": f"Bearer {access_token}"
        }) as session:
            async with session.put(
                    url="/user/edit",
                    data=json.dumps({
                        "email": login,
                        "password": password,
                        "ig_session_id": ig_session_id
                    })
            ) as response:
                print(f"edit:\nurl: {response.url}, \nstatus: {response.status}.")
                return response.status == 204
