"""
    @author Николай Витальевич Никоноров (Bitnik212)
    @date 02.03.2023 21:49
"""
from aiohttp.client import ClientSession
from yarl import URL

from reels_api_client.common.model.ReelsModel import ReelsModel
from reels_api_client.main.APIContext import APIContext


class ReelsAPI:

    PREFIX = "/reel"

    def __init__(self, context: APIContext):
        self.__client: ClientSession = context.session

    async def get_info(self, reel_code: str) -> ReelsModel | None:
        response = await self.__client.get(url=f"{self.PREFIX}/{reel_code}")
        if response.status == 200:
            return ReelsModel(**(await response.json())["data"])
        else:
            return None

    async def get_download_link(self, reel_link: str) -> str | None:
        response = await self.__client.get(url=URL(f"{self.PREFIX}/download/link?link={reel_link}", encoded=True))
        if response.status == 200:
            return (await response.json())["data"]["link"]
        else:
            return None
