"""
    @author Николай Витальевич Никоноров (Bitnik212)
    @date 02.03.2023 22:32
"""
from dataclasses import dataclass

from aiohttp import ClientSession

from reels_api_client.common.model.JWTsModel import JWTsModel
from reels_api_client.main.IJWTsCallback import IJWTsCallback


@dataclass
class APIContext:
    tokens: JWTsModel
    session: ClientSession
    jwt_callback: IJWTsCallback | None
    debug: bool = False
