__author__ = "Николай Витальевич Никоноров (Bitnik212)"
__date__ = "02.08.2023 23:43"

import asyncio
import json
from typing import Any, Optional, Iterable, Type, Union, List

import aiohttp
from aiohttp import ClientSession, BaseConnector, BasicAuth, ClientRequest, ClientResponse, ClientWebSocketResponse, \
    HttpVersion, http, ClientTimeout, TraceConfig
from aiohttp.abc import AbstractCookieJar
from aiohttp.client import _RequestContextManager
from aiohttp.helpers import sentinel
from aiohttp.typedefs import StrOrURL, LooseCookies, LooseHeaders, JSONEncoder

from reels_api_client.common.model.JWTsModel import JWTsModel
from reels_api_client.decorator.AuthInterceptor import AuthInterceptor
from reels_api_client.main.APIContext import APIContext
from reels_api_client.main.IJWTsCallback import IJWTsCallback


class CustomClientSession(ClientSession):

    def __init__(self,
                 tokens: JWTsModel,
                 jwt_callback: IJWTsCallback | None,
                 base_url: Optional[StrOrURL] = None,
                 *,
                 connector: Optional[BaseConnector] = None,
                 loop: Optional[asyncio.AbstractEventLoop] = None,
                 cookies: Optional[LooseCookies] = None,
                 headers: Optional[LooseHeaders] = None,
                 skip_auto_headers: Optional[Iterable[str]] = None,
                 auth: Optional[BasicAuth] = None,
                 json_serialize: JSONEncoder = json.dumps,
                 request_class: Type[ClientRequest] = ClientRequest,
                 response_class: Type[ClientResponse] = ClientResponse,
                 ws_response_class: Type[ClientWebSocketResponse] = ClientWebSocketResponse,
                 version: HttpVersion = http.HttpVersion11,
                 cookie_jar: Optional[AbstractCookieJar] = None,
                 connector_owner: bool = True,
                 raise_for_status: bool = False,
                 read_timeout: Union[float, object] = sentinel,
                 conn_timeout: Optional[float] = None,
                 timeout: Union[object, ClientTimeout] = sentinel,
                 auto_decompress: bool = True,
                 trust_env: bool = False,
                 requote_redirect_url: bool = True,
                 trace_configs: Optional[List[TraceConfig]] = None,
                 read_bufsize: int = 2 ** 16,
                 ):
        super().__init__(
            base_url,
            connector=connector,
            loop=loop,
            cookies=cookies,
            headers=headers,
            skip_auto_headers=skip_auto_headers,
            auth=auth,
            json_serialize=json_serialize,
            request_class=request_class,
            response_class=response_class,
            ws_response_class=ws_response_class,
            version=version,
            cookie_jar=cookie_jar,
            connector_owner=connector_owner,
            raise_for_status=raise_for_status,
            read_timeout=read_timeout,
            conn_timeout=conn_timeout,
            timeout=timeout,
            auto_decompress=auto_decompress,
            trust_env=trust_env,
            requote_redirect_url=requote_redirect_url,
            trace_configs=trace_configs,
            read_bufsize=read_bufsize
        )
        self.intercept = AuthInterceptor(
            context=APIContext(
                tokens=tokens,
                session=aiohttp.ClientSession(base_url=base_url, headers=headers),
                jwt_callback=jwt_callback
            )
        )

    async def get(self, url: StrOrURL, *, allow_redirects: bool = True, **kwargs: Any) -> "_RequestContextManager":
        return await self.intercept(super().get, url, allow_redirects=allow_redirects, **kwargs)
