"""
    @author Николай Витальевич Никоноров (Bitnik212)
    @date 02.03.2023 22:48
"""


class RenewTokensError(Exception):

    def __init__(self, message: str):
        super().__init__(message)

    def __str__(self) -> str:
        return super().__str__()
