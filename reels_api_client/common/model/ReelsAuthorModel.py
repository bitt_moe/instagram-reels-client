__author__ = "Николай Витальевич Никоноров (Bitnik212)"
__date__ = "03.08.2023 01:05"

from dataclasses import dataclass


@dataclass
class ReelsAuthorModel:
    user_id: str
    username: str
    full_name: str
    profile_pic_url: str
