"""
    @author Николай Витальевич Никоноров (Bitnik212)
    @date 02.03.2023 22:27
"""
from dataclasses import dataclass


@dataclass
class JWTsModel:
    access_token: str
    refresh_token: str
