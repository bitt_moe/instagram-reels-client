__author__ = "Николай Витальевич Никоноров (Bitnik212)"
__date__ = "03.08.2023 01:07"

from dataclasses import dataclass


@dataclass
class ReelsVideoModel:
    video_id: str
    width: int
    height: int
    url: str
