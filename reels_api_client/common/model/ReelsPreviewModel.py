__author__ = "Николай Витальевич Никоноров (Bitnik212)"
__date__ = "03.08.2023 01:06"

from dataclasses import dataclass


@dataclass
class ReelsPreviewModel:
    width: int
    height: int
    url: str
