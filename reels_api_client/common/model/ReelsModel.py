__author__ = "Николай Витальевич Никоноров (Bitnik212)"
__date__ = "03.08.2023 01:04"

from dataclasses import dataclass

from reels_api_client.common.model.ReelsAuthorModel import ReelsAuthorModel
from reels_api_client.common.model.ReelsPreviewModel import ReelsPreviewModel
from reels_api_client.common.model.ReelsVideoModel import ReelsVideoModel


@dataclass
class ReelsModel:

    def __init__(self,
                 media_id: str,
                 code: str,
                 description: str,
                 duration: float,
                 like_count: int,
                 view_count: int,
                 play_count: int,
                 author: dict,
                 previews: list[dict],
                 videos: list[dict]
                 ):
        self.media_id = media_id
        self.code = code
        self.description = description
        self.duration = duration
        self.like_count = like_count
        self.view_count = view_count
        self.play_count = play_count
        self.author = ReelsAuthorModel(**author)
        self.previews = [ReelsPreviewModel(**item) for item in previews]
        self.videos = [ReelsVideoModel(**item) for item in videos]

    media_id: str
    code: str
    description: str
    duration: float
    like_count: int
    view_count: int
    play_count: int
    author: ReelsAuthorModel
    previews: [ReelsPreviewModel]
    videos: [ReelsVideoModel]
