"""
    @author Николай Витальевич Никоноров (Bitnik212)
    @date 02.03.2023 21:36
"""
import inspect

from aiohttp import ClientResponse

from reels_api_client.main.APIContext import APIContext


class Logger:

    def __init__(self, context: APIContext):
        self.is_debug = context.debug

    def debug(self, message: any):
        if self.is_debug:
            print("-"*50)
            print(f"{self.__get_prefix()}: \n{message}")
            print("-"*50)

    @staticmethod
    def __get_prefix(level: int = 2) -> str:
        frame = inspect.currentframe()
        for i in range(level):
            frame = frame.f_back
        return inspect.getmodule(frame).__name__

    @staticmethod
    async def parse_response(response: ClientResponse) -> str:
        return f"status: {response.status}, \nbody: {await response.text()}, \nheaders: {dict(response.headers)}\n"
